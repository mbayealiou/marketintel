from django.shortcuts import render
from dashboard.models import Offre
from django.core.paginator import Paginator
from django.db.models import Count

def index(request):
    offres_list = Offre.objects.all()
    paginator = Paginator(offres_list,6)

    page = request.GET.get('page')
    offres = paginator.get_page(page)

    #stats
    nbreOffres = offres_list.count()

    nbrePays = Offre.objects.all().values_list('pays').distinct().count() - 1

    secteurs = ["informatique","Santé","Commerce","Gestion","Agriculture","Restauration","Transport","Justice","Administration","Environnement","Vente","Ressources Humaines","Industrie","Marketing","Services","Batiment"]
    ns = 0
    for sect in secteurs:
        i = Offre.objects.filter(secteur__contains=sect).count()
        if i!=0:
            ns = ns + 1

    #type contrat
    nbreCDI = Offre.objects.filter(typeContrat__contains="CDI").count()
    pourcentageCDI = round(nbreCDI/nbreOffres*100)

    nbreCDD = Offre.objects.filter(typeContrat__contains="CDD").count()
    pourcentageCDD = round(nbreCDD/nbreOffres*100)

    nbreStage = Offre.objects.filter(typeContrat__contains="Stage").count()
    pourcentageStage = round(nbreStage/nbreOffres*100)

    nbreAlternance = Offre.objects.filter(typeContrat__contains="Alternance").count()
    pourcentageAlternance = round(nbreAlternance/nbreOffres*100)

    nbreFreelance = Offre.objects.filter(typeContrat__contains="Freelance").count()
    pourcentageFreelance = round(nbreCDI/nbreOffres*100)


    #secteur
    nbreInf = Offre.objects.filter(secteur__contains="Informatique").count()
    pourcentageInf = round(nbreInf/nbreOffres*100)

    nbreVente = Offre.objects.filter(secteur__contains="Vente").count()
    pourcentageVente = round(nbreVente/nbreOffres*100)

    nbreBatiment = Offre.objects.filter(secteur__contains="Batiment").count()
    pourcentageBatiment = round(nbreBatiment/nbreOffres*100)

    nbreInd = Offre.objects.filter(secteur__contains="Electrique").count()
    pourcentageInd = round(nbreInd/nbreOffres*100)

    nbreSante = Offre.objects.filter(secteur__contains="Sante").count()
    pourcentageSante = round(nbreSante/nbreOffres*100)

    nbrEnv = Offre.objects.filter(secteur__contains="Agriculture").count()
    pourcentageEnv = round(nbrEnv/nbreOffres*100)

    return render(request,"index.html",{'offres': offres, 'pays': pays,'no': nbreOffres,'np': nbrePays,'ns': ns,'cdi': pourcentageCDI, 'cdd': pourcentageCDD,'stage': pourcentageStage,'alternance': pourcentageAlternance,'freelance': pourcentageFreelance,'inf': pourcentageInf,'vente': pourcentageVente,'batiment': pourcentageBatiment,'industrie': pourcentageInd,'sante': pourcentageSante,'env': pourcentageEnv})


def pays(request,p):
    offres_list = Offre.objects.filter(pays=p)
    paginator = Paginator(offres_list,10)

    page = request.GET.get('page')
    offres = paginator.get_page(page)

    pays = p

    #stats
    nbreOffres = offres_list.count()

    nbrePays = 1

    secteurs = ["informatique","Santé","Commerce","Gestion","Agriculture","Restauration","Transport","Justice","Administration","Environnement","Vente","Ressources Humaines","Industrie","Marketing","Services","Batiment"]
    ns = 0
    for sect in secteurs:
        i = Offre.objects.filter(secteur__contains="Informatique",pays=p).count()
        if i!=0:
            ns = ns + 1


    #type contrat
    nbreCDI = Offre.objects.filter(typeContrat__contains="CDI").filter(pays=p).count()
    pourcentageCDI = round(nbreCDI/nbreOffres*100)

    nbreCDD = Offre.objects.filter(typeContrat__contains="CDD").filter(pays=p).count()
    pourcentageCDD = round(nbreCDD/nbreOffres*100)

    nbreStage = Offre.objects.filter(typeContrat__contains="Stage").filter(pays=p).count()
    pourcentageStage = round(nbreStage/nbreOffres*100)

    nbreAlternance = Offre.objects.filter(typeContrat__contains="Alternance").filter(pays=p).count()
    pourcentageAlternance = round(nbreAlternance/nbreOffres*100)

    nbreFreelance = Offre.objects.filter(typeContrat__contains="Freelance").filter(pays=p).count()
    pourcentageFreelance = round(nbreCDI/nbreOffres*100)


    #secteur
    nbreInf = Offre.objects.filter(secteur__contains="Informatique").count()
    pourcentageInf = round(nbreInf/nbreOffres*100)

    nbreVente = Offre.objects.filter(secteur__contains="Vente").count()
    pourcentageVente = round(nbreVente/nbreOffres*100)

    nbreBatiment = Offre.objects.filter(secteur__contains="Batiment").count()
    pourcentageBatiment = round(nbreBatiment/nbreOffres*100)

    nbreInd = Offre.objects.filter(secteur__contains="Electrique").count()
    pourcentageInd = round(nbreInd/nbreOffres*100)

    nbreSante = Offre.objects.filter(secteur__contains="Sante").count()
    pourcentageSante = round(nbreSante/nbreOffres*100)

    nbrEnv = Offre.objects.filter(secteur__contains="Agriculture").count()
    pourcentageEnv = round(nbrEnv/nbreOffres*100)

    return render(request,"pays.html",{'offres': offres, 'pays': pays,'no': nbreOffres,'np': nbrePays,'ns': ns,'cdi': pourcentageCDI, 'cdd': pourcentageCDD,'stage': pourcentageStage,'alternance': pourcentageAlternance,'freelance': pourcentageFreelance,'inf': pourcentageInf,'vente': pourcentageVente,'batiment': pourcentageBatiment,'industrie': pourcentageInd,'sante': pourcentageSante,'env': pourcentageEnv})


def secteur(request,s):

    offres_list = Offre.objects.filter(secteur__contains=s)
    paginator = Paginator(offres_list,10)

    page = request.GET.get('page')
    offres = paginator.get_page(page)

    secteur = s

    #stats
    nbreOffres = offres_list.count()

    nbrePays = offres_list.values_list('pays').distinct().count() - 1

    nbreSecteur = 1

    #type contrat
    nbreCDI = Offre.objects.filter(typeContrat__contains="CDI").filter(secteur__contains=s).count()
    pourcentageCDI = round(nbreCDI/nbreOffres*100)

    nbreCDD = Offre.objects.filter(typeContrat__contains="CDD").filter(secteur__contains=s).count()
    pourcentageCDD = round(nbreCDD/nbreOffres*100)

    nbreStage = Offre.objects.filter(typeContrat__contains="Stage").filter(secteur__contains=s).count()
    pourcentageStage = round(nbreStage/nbreOffres*100)

    nbreAlternance = Offre.objects.filter(typeContrat__contains="Alternance").filter(secteur__contains=s).count()
    pourcentageAlternance = round(nbreAlternance/nbreOffres*100)

    nbreFreelance = Offre.objects.filter(typeContrat__contains="Freelance").filter(secteur__contains=s).count()
    pourcentageFreelance = round(nbreCDI/nbreOffres*100)


    #secteur
    nbreInf = Offre.objects.filter(secteur__contains="Informatique").count()
    pourcentageInf = round(nbreInf/nbreOffres*100)

    nbreVente = Offre.objects.filter(secteur__contains="Vente").count()
    pourcentageVente = round(nbreVente/nbreOffres*100)

    nbreBatiment = Offre.objects.filter(secteur__contains="Batiment").count()
    pourcentageBatiment = round(nbreBatiment/nbreOffres*100)

    nbreInd = Offre.objects.filter(secteur__contains="Electrique").count()
    pourcentageInd = round(nbreInd/nbreOffres*100)

    nbreSante = Offre.objects.filter(secteur__contains="Sante").count()
    pourcentageSante = round(nbreSante/nbreOffres*100)

    nbrEnv = Offre.objects.filter(secteur__contains="Agriculture").count()
    pourcentageEnv = round(nbrEnv/nbreOffres*100)

    return render(request,"secteur.html",{'offres': offres, 'secteur': secteur,'no': nbreOffres,'np': nbrePays,'ns': nbreSecteur, 'cdi': pourcentageCDI, 'cdd': pourcentageCDD,'stage': pourcentageStage,'alternance': pourcentageAlternance,'freelance': pourcentageFreelance,'inf': pourcentageInf,'vente': pourcentageVente,'batiment': pourcentageBatiment,'industrie': pourcentageInd,'sante': pourcentageSante,'env': pourcentageEnv})


def pays_secteur(request,p,s):

    offres_list = Offre.objects.filter(secteur__contains=s,pays=p)
    paginator = Paginator(offres_list,10)

    page = request.GET.get('page')
    offres = paginator.get_page(page)

    secteur = s
    pays = p



    #stats
    nbreOffres = offres_list.count()

    nbrePays = 1

    nbreSecteur = 1

    #type contrat
    nbreCDI = Offre.objects.filter(typeContrat__contains="CDI").filter(secteur__contains=s).filter(pays=pays).count()
    pourcentageCDI = round(nbreCDI/nbreOffres*100)

    nbreCDD = Offre.objects.filter(typeContrat__contains="CDD").filter(secteur__contains=s).filter(pays=pays).count()
    pourcentageCDD = round(nbreCDD/nbreOffres*100)

    nbreStage = Offre.objects.filter(typeContrat__contains="Stage").filter(secteur__contains=s).filter(pays=pays).count()
    pourcentageStage = round(nbreStage/nbreOffres*100)

    nbreAlternance = Offre.objects.filter(typeContrat__contains="Alternance").filter(secteur__contains=s).filter(pays=pays).count()
    pourcentageAlternance = round(nbreAlternance/nbreOffres*100)

    nbreFreelance = Offre.objects.filter(typeContrat__contains="Freelance").filter(secteur__contains=s).filter(pays=pays).count()
    pourcentageFreelance = round(nbreCDI/nbreOffres*100)


    #secteur
    nbreInf = Offre.objects.filter(secteur__contains="Informatique").count()
    pourcentageInf = round(nbreInf/nbreOffres*100)

    nbreVente = Offre.objects.filter(secteur__contains="Vente").count()
    pourcentageVente = round(nbreVente/nbreOffres*100)

    nbreBatiment = Offre.objects.filter(secteur__contains="Batiment").count()
    pourcentageBatiment = round(nbreBatiment/nbreOffres*100)

    nbreInd = Offre.objects.filter(secteur__contains="Electrique").count()
    pourcentageInd = round(nbreInd/nbreOffres*100)

    nbreSante = Offre.objects.filter(secteur__contains="Sante").count()
    pourcentageSante = round(nbreSante/nbreOffres*100)

    nbrEnv = Offre.objects.filter(secteur__contains="Agriculture").count()
    pourcentageEnv = round(nbrEnv/nbreOffres*100)

    return render(request,"pays_secteur.html",{'offres': offres, 'secteur': secteur, 'pays': p,'no': nbreOffres,'np': nbrePays,'ns': nbreSecteur,'cdi': pourcentageCDI, 'cdd': pourcentageCDD,'stage': pourcentageStage,'alternance': pourcentageAlternance,'freelance': pourcentageFreelance,'inf': pourcentageInf,'vente': pourcentageVente,'batiment': pourcentageBatiment,'industrie': pourcentageInd,'sante': pourcentageSante,'env': pourcentageEnv})


def rechercheInt(request):

    offres_list = Offre.objects.filter(intitule__contains=request.POST.get('intitule'))
    paginator = Paginator(offres_list,5)

    page = request.GET.get('page')
    offres = paginator.get_page(page)

    return render(request,"recherche.html",{'offres': offres})


"""
def rechercheSect(request):
    if request.method=='POST':
        if Offre.objects.filter(secteur__contains=request.POST.get('secteur')).count != 0:
            offres_list = Offre.objects.filter(secteur__contains=request.POST.get('secteur'))
        else:
            return render(request,"rechercheSR.html")

    paginator = Paginator(offres_list,10)

    page = request.GET.get('page')
    offres = paginator.get_page(page)

    secteur = request.POST.get('secteur')
    s = request.POST.get('secteur')

    #stats
    nbreOffres = offres_list.count()

    nbrePays = offres_list.values_list('pays').distinct().count()

    nbreSecteur = 1

    #type contrat
    nbreCDI = Offre.objects.filter(typeContrat__contains="CDI").filter(secteur__contains=s).count()
    pourcentageCDI = round(nbreCDI/nbreOffres*100)

    nbreCDD = Offre.objects.filter(typeContrat__contains="CDD").filter(secteur__contains=s).count()
    pourcentageCDD = round(nbreCDD/nbreOffres*100)

    nbreStage = Offre.objects.filter(typeContrat__contains="Stage").filter(secteur__contains=s).count()
    pourcentageStage = round(nbreStage/nbreOffres*100)

    nbreAlternance = Offre.objects.filter(typeContrat__contains="Alternance").filter(secteur__contains=s).count()
    pourcentageAlternance = round(nbreAlternance/nbreOffres*100)

    nbreFreelance = Offre.objects.filter(typeContrat__contains="Freelance").filter(secteur__contains=s).count()
    pourcentageFreelance = round(nbreCDI/nbreOffres*100)

    #secteur
    nbreInf = Offre.objects.filter(secteur__contains="Informatique").count()
    pourcentageInf = round(nbreInf/nbreOffres*100)

    nbreVente = Offre.objects.filter(secteur__contains="Vente").count()
    pourcentageVente = round(nbreVente/nbreOffres*100)

    nbreBatiment = Offre.objects.filter(secteur__contains="Batiment").count()
    pourcentageBatiment = round(nbreBatiment/nbreOffres*100)

    nbreInd = Offre.objects.filter(secteur__contains="Electrique").count()
    pourcentageInd = round(nbreInd/nbreOffres*100)

    nbreSante = Offre.objects.filter(secteur__contains="Sante").count()
    pourcentageSante = round(nbreSante/nbreOffres*100)

    nbrEnv = Offre.objects.filter(secteur__contains="Agriculture").count()
    pourcentageEnv = round(nbrEnv/nbreOffres*100)

    return render(request,"secteur.html",{'offres': offres, 'secteur': secteur,'no': nbreOffres,'np': nbrePays,'ns': nbreSecteur, 'cdi': pourcentageCDI, 'cdd': pourcentageCDD,'stage': pourcentageStage,'alternance': pourcentageAlternance,'freelance': pourcentageFreelance,'inf': pourcentageInf,'vente': pourcentageVente,'batiment': pourcentageBatiment,'industrie': pourcentageInd,'sante': pourcentageSante,'env': pourcentageEnv})
"""
